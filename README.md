## What is this?

This repository contains several projects which I worked on in an attempt to mod Stick Ranger 1 & 2.
Some of them were more effective, others less.

 1. (not included in this repository):
	I was first interested in modding SR2, which I have since stopped attempting (for obvious reasons).
	My initial idea was to create a modding interface, which would require minimal work to hook into the game after every update. This actually worked relatively well, I think that there are screenshots in the Discord if you want to see the results.
	After everything I tried, I think that this is the best (and only) way forward for anyone who wants to mod SR2 (other than waiting for the final release)

 2. (not included in this repository):
	After seeing how much work it would be to do option 1., I decided to try simply using a JS deobfuscator, figuring out how the code worked, and writing a port by hand. This ironically would end up being way more work than option 1.
	It should be noted that at this point I was still not aware of the discord or the existing decompile.
	This ended up being a lot of work of course, and as anyone who has tried something like this should know, the publicly available deobfuscators have a very hard time with DB code, only fixing maybe 20-30% of what they could be fixing.
	I eventually deobfuscated (by hand) roughly 80% of the SR2 codebase. I would later try to port this to another language by hand with little success.
	I also tried this with SR1, but with it having almost an order of magnitude more LOC, that obviously wasn't going to happen.

 3. (not included in this repository):
	Around this time I ended up finding the Discord server. I started messing with AST transforms, trying to naively translate JS AST to Odin code. It worked okay in the sense that I was able to get the first few screens to work without crashing, the code was just as ugly has the source code. Even if I was able to get the whole game running, it would be of no use for modding (which is my end goal for all of these attempts).

 4. After these attempts I made a final attempt to get SR2 to work. I am generally much more interested in SR2, so I really wanted to find a magic solution that would let someone mod the game across game versions. This is where my ideas started to get more complicated. The idea was that I would again use an AST parser, but this time keep track of the name of each function, its number of arguments, the size in characters, and how many literals of different types were used within. I would then save this data to a file, in the hopes that I could track functions across versions. The idea was that even though the function names changed, the behavior of most functions would stay mostly the same.
	I think that this idea has merit in theory, but in practice doing this correctly would be a massive project. Again for SR2 modding, I'd suggest idea #1.
	- If you're interested in the code, most of it exists in the `src/rename_transform` folder.

 5. Finally (ish), after learning how to use the AST library (babel), I started messing with it to see if I could fix the issues which the previously mentioned online deobfuscators had. This ended up being by far my most successful attempt (in a sense). To make a long story short, not only did I fix many of the issues these other deobfuscators had, I also effectively reimplemented their techniques as well, allowing a one step process to get a very good deobfuscation of any DB game. It should be noted that this will probably only work well against DB games, each fix was implemented specifically to deal with such a game (SR1 mostly).
	I also worked on an Odin emitter. It's very WIP but does emit quite plausible code.

I don't want to upload actual game code. If you want to try this project, then put an original .js file from a DB game in `input/`, change the paths in `index.js` to convert it, and run the script.
If you want to see the output without doing this then ask and I can provide.

## Going forward

Given my experience with these projects, my personal opinion as to the best way to make SR1 moddable would be the following:
 1. Work on the deobfuscator more, get it from 80% complete to 90% or so.
 2. Make a new dewrite based off of this code. (Ie. variable/function renames + comments only)
	- NOTE: I know that there are some other projects looking to do similar things. I don't want to discourage anyone from trying other methods, but I myself think that it's very important that __100% code compatibility__ is maintained. This means that not only should the code be functionally identical, ideally no step of the porting process should be able to introduce mistakes.
 3. This dewrite should still be functionally equivalent to the original code. This is useful because up to this point, the game will always be able to be run on the main website, so it can be continually tested.
 4. This dewrite should then be ported incrementally to a better language. From my experience it seems that C is the best choice here for a multitude of reasons. This part would be dangerous, as the game as a whole cannot be tested until this is complete.
 5. (speculation, getting to this point would take a long time). At this point the fun could begin. Rewrites of systems for readability and performance, proper modding support, potentially multiplayer support etc.

Anyway, thanks for coming to my TED talk.

I don't intend for this to be any sort of call to action, I just thought that it might be of benefit to others to have this information out there. Ask me if you have any questions.
