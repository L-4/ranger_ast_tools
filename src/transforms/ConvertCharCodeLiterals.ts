import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class ConvertCharCodeLiterals extends ITransform {
    public get_description(): string {
        return "Transform with no description";
    }

    public constructor() {
        super();
        this.traverse_options = {
            CallExpression: path => {
                const node = path.node;
                if (!t.isIdentifier(node.callee)) return

                if (node.callee.name != "from_char_code") return;

                let str = "";
                for (const argument of node.arguments) {
                    if (!t.isNumericLiteral(argument)) return;
                    if (argument.value != (argument.value | 0)) return; // Not integer

                    str += String.fromCharCode(argument.value);
                }

                // @ts-ignore
                path.replaceWith({ type: "StringLiteral", value: str });
            },
        };
    }
}
