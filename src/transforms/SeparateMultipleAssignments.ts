import * as t from "@babel/types";
import { ITransform } from "./ITransform";
import { NodePath } from "babel-traverse";
import { err_loc } from "../utils";

export class SeparateMultipleAssignments extends ITransform {
    public get_description(): string {
        return "Separates multiple assignments.";
    }

    private try_fix_multi_assign(parent_path: NodePath, base_assignment: t.AssignmentExpression, strategy: "push-pre" | "replace-multiple"): boolean {
        if (!t.isAssignmentExpression(base_assignment.right)) {
            // Early out - single assignment cannot be split further.
            return false;
        }

        let rightmost = base_assignment;
        const all_assignments = [rightmost];

        while (t.isAssignmentExpression(rightmost.right)) {
            // We're an assigment, as well as the next. Continue.
            rightmost = rightmost.right;
            all_assignments.push(rightmost);
        }

        // We're an expression, our right is some type of a value.
        const value = rightmost.right;

        if (!t.isBooleanLiteral(value) &&
            !t.isNumericLiteral(value) &&
            !t.isMemberExpression(value) &&
            !t.isIdentifier(value)) {
            // If you ended up here, add your type to the above whitelist, and make sure that it plays nice!
            err_loc(value.loc, `Failed convert multiple assignments into one since the rhs was a ${value.type}`)
            return false;
        }

        const new_assignments = [];

        for (const assignment of all_assignments) {
            if (assignment.operator !== "=") {
                throw "Invalid operator in assignment!";
            }
            new_assignments.push(t.expressionStatement(t.assignmentExpression("=", assignment.left, value)));
        }

        if (strategy === "replace-multiple") {
            parent_path.replaceWithMultiple(new_assignments);
            return true;
        } else {
            parent_path.insertBefore(new_assignments);
            return true;
        }
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: path => {
                const base_assignment = path.node.expression;
                if (!t.isAssignmentExpression(base_assignment)) {
                    return;
                }

                this.try_fix_multi_assign(path, base_assignment, "replace-multiple");
            },
            ForStatement: path => {
                const base_assignment = path.node.init;
                if (base_assignment === null || !t.isAssignmentExpression(base_assignment)) {
                    return;
                }

                if (this.try_fix_multi_assign(path, base_assignment, "push-pre")) {
                    path.node.init = null;
                }
            }
        };
    }
}
