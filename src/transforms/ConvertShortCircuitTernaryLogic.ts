// @ts-nocheck
import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { separate_if_sequence } from "../utils";

export class ConvertShortCircuitTernaryLogic extends ITransform {
    public get_description(): string {
        return "Convert short circuit logic and ternary to if/else";
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: (path) => {
                const expression = path.node.expression;
                if (t.isLogicalExpression(expression)) {
                    if (expression.operator === "&&") {
                        path.replaceWith(t.ifStatement(
                            expression.left,
                            t.blockStatement(separate_if_sequence(expression.right))));
                    } else if (expression.operator === "||") {
                        path.replaceWith(t.ifStatement(
                            t.unaryExpression("!", expression.left, true),
                            t.blockStatement(separate_if_sequence(expression.right))));
                    }
                } else if (t.isConditionalExpression(expression)) {
                    path.replaceWith(t.ifStatement(
                        expression.test,
                        t.blockStatement(separate_if_sequence(expression.consequent)),
                        t.blockStatement(separate_if_sequence(expression.alternate))));
                }
            },
        };
    }
}
