// @ts-nocheck
import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

export class ChainedIfToSwitch extends ITransform {
    public get_description(): string {
        return "Convert chained simple if statements into switch statements";
    }

    public constructor() {
        super();
        this.traverse_options = {
            IfStatement: path => {
                let cond = path.node;

                if (path.parentPath.node.type === "IfStatement") {
                    console.log("Parent was if");
                    return;
                }

                if (!cond.alternate) {
                    console.log("Has no alternate");
                    return;
                }

                let was_simple = true;
                let id;
                const cases = [];

                while (true) {
                    const test = cond.test;
                    if (test.type !== "BinaryExpression") {
                        console.log("Was not simple: " + "Test was not binary expression");
                        was_simple = false;
                        break;
                    }

                    if (test.operator !== "==" && test.operator !== "===") {
                        console.log("Was not simple: " + "Invalid operator");
                        was_simple = false;
                        break;
                    }

                    let lval = test.left;
                    let rval = test.right;

                    // Ensure that any valid statement has a numeric literal on the right.
                    if (lval.type === "NumericLiteral") {
                        [lval, rval] = [rval, lval];
                    }

                    if (lval.type !== "Identifier" || rval.type !== "NumericLiteral") {
                        console.log("Was not simple: " + "Not simple binary expression");
                        was_simple = false;
                        break;
                    }

                    if (!id) {
                        id = lval;
                    } else if (lval.name !== id.name) {
                        console.log("Was not simple: " + "Different identifier");
                        was_simple = false;
                        break;
                    }

                    const consequent = cond.consequent;
                    if (consequent.type !== "BlockStatement") {
                        console.warn("Assuming that consequent must be block statement. Skipping...");
                        was_simple = false;
                        break;
                    }

                    consequent.body.push({ type: "BreakStatement" });

                    cases.push({
                        type: "SwitchCase",
                        test: rval,
                        consequent: consequent,
                    });

                    if (!cond.alternate) {
                        break;
                    }

                    const alternate = cond.alternate;
                    if (alternate.type === "BlockStatement") {
                        alternate.body.push({ type: "BreakStatement" });
                        cases.push({
                            type: "SwitchCase",
                            consequent: alternate,
                        });
                        break;
                    } else if (alternate.type !== "IfStatement") {
                        console.log("Was not simple: " + "Alternate was not if statement");
                        was_simple = false;
                        break;
                    }

                    cond = cond.alternate;
                }

                if (was_simple) {
                    path.replaceWith({
                        type: "SwitchStatement",
                        discriminant: id,
                        cases: cases,
                    });
                }

                console.log("Was simple: " + was_simple);
            },
        };
    }
}
