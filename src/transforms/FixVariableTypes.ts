import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

// This is experimental as I am not at all sure that path.parentPath.traverse
// is safe. What if parentPath is an expressionStatatement?
// What if it's a block but not a scope?
// This still has issues with shadowing variables
// @todo: Shadowing seems to only happen within the same scope? Try searching for multiple
// variable declarations as well.

// Commented out because I don't know that it works if multiple variables are assigned at once.

// export class FixVariableTypes extends ITransform {
//     public get_description(): string {
//         return "Convert variable declarations to `var`, `let`, or `const`, contextually";
//     }

//     public constructor() {
//         super();
//         this.traverse_options = {
//             VariableDeclaration: (path) => {
//                 if (path.node.declarations.length > 1) {
//                     console.error("Multiple declarations found! Skipping.");
//                     return;
//                 }

//                 const declaration = path.node.declarations[0];
//                 if (declaration.id.type !== "Identifier") {
//                     console.warn(`L value of variable declaration was not identifier! (was ${declaration.id.type}). Skipping.`);
//                     return;
//                 }

//                 let assignment_found = false;
//                 let first_declaration_found = false;
//                 let failure_type;

//                 const var_name = declaration.id.name;
//                 path.parentPath.traverse({
//                     AssignmentExpression: (p) => {
//                         if (assignment_found) {
//                             return;
//                         } else if (p.node.left.type === "Identifier" && p.node.left.name === var_name) {
//                             assignment_found = true;
//                             failure_type = "assignment";
//                             return;
//                         }
//                     },
//                     UpdateExpression: (p) => {
//                         if (assignment_found) {
//                             return;
//                         }
//                         if (p.node.argument.type !== "Identifier") {
//                             return;
//                         }
//                         if (p.node.argument.name === var_name) {
//                             assignment_found = true;
//                             failure_type = "assignment";
//                             return;
//                         }
//                     },
//                     VariableDeclaration: (p) => {
//                         if (assignment_found) {
//                             return;
//                         }

//                         for (const declaration of p.node.declarations) {
//                             if (declaration.id.type !== "Identifier") {
//                                 throw "Rock";
//                             }

//                             if (declaration.id.name === var_name) {
//                                 if (first_declaration_found) {
//                                     assignment_found = true;
//                                     failure_type = "variable";
//                                     return;
//                                 } else {
//                                     first_declaration_found = true;
//                                 }
//                             }
//                         }
//                     },
//                 });

//                 if (!assignment_found) {
//                     path.node.kind = "const";
//                 } else {
//                     path.node.kind = failure_type === "assignment" ? "let" : "var";
//                 }
//             },
//         };
//     }
// }
