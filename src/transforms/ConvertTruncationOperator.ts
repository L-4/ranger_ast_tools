import * as t from "@babel/types";
import { ITransform } from "./ITransform";

// function truncate(val) { return ~~val; }
// @todo: Auto inject truncate definition
export class ConvertTruncationOperator extends ITransform {
    public get_description(): string {
        return "Convers '~~' to calls to truncate()";
    }

    public constructor() {
        super();
        this.traverse_options = {
            UnaryExpression: path => {
                const node = path.node;
                if (node.operator !== "~" || !node.prefix) return;
                const arg = node.argument;

                if (!t.isUnaryExpression(arg)) return;
                if (arg.operator !== "~" || !arg.prefix) return;

                path.replaceWith({
                    type: "CallExpression",
                    // @ts-ignore
                    callee: { type: "Identifier", name: "truncate" },
                    arguments: [ arg.argument ],
                });
            },
        };
    }
}
