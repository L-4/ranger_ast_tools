// @ts-nocheck
import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class InsertReflectionCalls extends ITransform {
    public get_description(): string {
        return "Insert reflection calls";
    }

    private get_type_call(name: string): t.Expression {
        return {
            type: "ExpressionStatement",
            expression: {
                type: "CallExpression",
                callee: {
                    type: "Identifier",
                    name: "value_set_as",
                },
                arguments: [
                    { type: "Identifier", name: name },
                    { type: "StringLiteral", value: name },
                ]
            }
        }
    }

    private get_type_call_in_func(name: string, func: string): t.Expression {
        return {
            type: "ExpressionStatement",
            expression: {
                type: "CallExpression",
                callee: {
                    type: "Identifier",
                    name: "value_set_as_in_func",
                },
                arguments: [
                    { type: "Identifier", name: name },
                    { type: "StringLiteral", value: name },
                    { type: "StringLiteral", value: func },
                ]
            }
        }
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: path => {
                const node = path.node.expression;

                if (t.isAssignmentExpression(node)) {
                    if (node.operator != "=") return;
                    if (!t.isIdentifier(node.left)) return;

                    path.insertAfter(this.get_type_call(node.left.name));
                }
            },

            FunctionDeclaration: path => {
                const node = path.node;
                if (node.params.length == 0) return;
                const new_body = [];

                for (const param of node.params) {
                    if (!t.isIdentifier(param)) continue;
                    new_body.push(this.get_type_call_in_func(param.name, node.id.name));
                }

                if (new_body.length == 0) return;
                new_body.push(...node.body.body);

                node.body.body = new_body;
            },

            VariableDeclaration: path => {
                const node = path.node;

                for (const declaration of node.declarations) {
                    if (!t.isIdentifier(declaration.id)) continue;
                    if (declaration.init === undefined) continue;

                    path.insertAfter(this.get_type_call(declaration.id.name));
                }
            },
        };
    }
}
