// @ts-nocheck
import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class TestTransform extends ITransform {
    public get_description(): string {
        return "Transform with no description";
    }

    public constructor() {
        super();
        this.traverse_options = {
            // UnaryExpression: path => {
            //     const node = path.node;
            //     if (node.operator !== "~" || !node.prefix) return;
            //     const arg = node.argument;

            //     if (!t.isUnaryExpression(arg)) return;
            //     if (arg.operator !== "~" || !arg.prefix) return;

            //     path.replaceWith({
            //         type: "CallExpression",
            //         callee: { type: "Identifier", name: "truncate" },
            //         arguments: [ arg.argument ],
            //     });
            // },

            // BinaryExpression: path => {
            //     const node = path.node;
            //     if (node.operator !== ">>>") return;
            //     const [left, right] = [node.left, node.right];
            //     if (!t.isNumericLiteral(right) || right.value !== 0) throw "Asd";

            //     path.replaceWith({
            //         type: "CallExpression",
            //         callee: { type: "Identifier", name: "truncate_unsigned" },
            //         arguments: [ left ],
            //     });
            // }
        };
    }
}
