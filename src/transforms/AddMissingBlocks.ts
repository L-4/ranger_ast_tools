// @ts-nocheck
import * as t from "@babel/types";
import { Node } from "@babel/types";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { TraverseOptions } from "babel-traverse";
import { node_default } from "../utils";

export class AddMissingBlocks extends ITransform {
    public get_description(): string {
        return "Add blocks to statements missing them";
    }

    public constructor() {
        super();
        this.traverse_options = {
            enter: (path) => {
                if (path.isForInStatement()
                    || path.isForOfStatement()
                    || path.isForStatement()) {
                    if (path.node.body.type !== "BlockStatement") {
                        path.node.body = {
                            ...node_default,
                            type: "BlockStatement",
                            body: [path.node.body],
                        }
                    }
                } else if (path.isIfStatement()) {
                    if (path.node.consequent.type !== "BlockStatement") {
                        path.node.consequent = {
                            ...node_default,
                            type: "BlockStatement",
                            body: [path.node.consequent],
                        };
                    }

                    if (path.node.alternate !== null) {
                        const alternate_type = path.node.alternate.type;
                        if (alternate_type !== "BlockStatement" && alternate_type !== "IfStatement") {
                            path.node.alternate = {
                                ...node_default,
                                type: "BlockStatement",
                                body: [path.node.alternate],
                            };
                        }
                    }
                }
            },
        };
    }
}

