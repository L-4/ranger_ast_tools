import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

export class RemoveCommaOperatorFromIfStatement extends ITransform {
    public get_description(): string {
        return "Remove comma operator from if statement";
    }

    public constructor() {
        super();
        this.traverse_options = {
            IfStatement: path => {
                const node = path.node;
                const test = node.test;
                if (test.type === "SequenceExpression") {
                    if (test.expressions.length <= 1) {
                        throw "Rock";
                    }
                    // We have to move out the first expressions as to keep the evaluation order
                    const extra_expressions = test.expressions.slice(0, -1);
                    for (let i = 0; i < extra_expressions.length; ++i) {
                        const old_expression = extra_expressions[i];
                        extra_expressions[i] = {
                            // @ts-ignore
                            type: "ExpressionStatement",
                            expression: old_expression,
                        };
                    }
                    const last_expression = test.expressions[test.expressions.length - 1];
                    node.test = last_expression;

                    path.insertBefore(extra_expressions);
                }
            }
        };
    }
}
