import * as t from "@babel/types";
import { BinaryExpression, Expression } from "@babel/types";
import { ITransform } from "./ITransform";

type BinaryOperator = BinaryExpression["operator"];

/**
 * Swaps around the order of operators when there's a literal on the left:
 * `if (10 < a) -> if (a > 10)`, `if (null == e) -> `if (e == null)`
 */
export class SwapOperatorSides extends ITransform {
    public get_description(): string {
        return "Swap operators such that the literal is on the right";
    }

    private static readonly simple_swap_operators: BinaryOperator[] = ["==", "===", "!=", "!==", "+", "*"];
    private static readonly swap_pairs: [BinaryOperator, BinaryOperator][] = [["<", ">"], [">", "<"], ["<=", ">="], [">=", "<="]];


    /**
     * Returns whether the node is either directly a literal, or a simple enough
     * binary/unary expression to be considered one.
     */
    private node_is_literal(node: Node): boolean {
        if (t.isNumericLiteral(node) ||
            t.isNullLiteral(node)) {
            return true;
        }

        if (t.isUnaryExpression(node) && node.operator == "-") {
            if (t.isNumericLiteral(node.argument)) {
                return true;
            }
        }

        return false;
    }

    public constructor() {
        super();
        this.traverse_options = {
            BinaryExpression: (path) => {
                const node = path.node;

                // @ts-ignore
                if (this.node_is_literal(node.left)) {
                    if (SwapOperatorSides.simple_swap_operators.includes(node.operator)) {
                        const temp = node.left;
                        node.left = node.right;
                        node.right = temp;

                        return;
                    }

                    for (const pair of SwapOperatorSides.swap_pairs) {
                        if (node.operator === pair[0]) {
                            const temp = node.left;
                            node.left = node.right;
                            node.right = temp;
                            node.operator = pair[1];

                            return;
                        }
                    }
                }
            },
        };
    }
}
