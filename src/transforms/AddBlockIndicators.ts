import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class AddBlockIndicators extends ITransform {
    public get_description(): string {
        return "Adds indicators to the start and end of blocks";
    }

    public constructor() {
        super();
        this.traverse_options = {
            Statement: path => {
                path.node.leadingComments = [
                    // @ts-ignore
                    { type: "CommentBlock", value: "__BEGIN_STATEMENT__" },
                ]
                path.node.trailingComments = [
                    // @ts-ignore
                    { type: "CommentBlock", value: "__END_STATEMENT__" },
                ];
            },
        };
    }
}
