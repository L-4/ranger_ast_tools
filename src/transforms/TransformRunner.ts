import * as t from "@babel/types";
import { ITransform } from "./ITransform";
import * as fs from "fs";
import generate from "@babel/generator";
import { parse } from "@babel/parser";
import { File } from "@babel/types";

type RegexReplacement = [RegExp, string];

export class TransformRunner {
    public constructor(
        private readonly transforms: ITransform[] = [],
        private readonly replacements: RegexReplacement[] = [],
    ) { }

    protected active_file: File;
    private run_transforms: string[] = [];

    public run(input_file: string, output_file: string): void {

        const contents = fs.readFileSync(input_file, "utf-8");
        this.active_file = parse(contents, {
            sourceFilename: input_file,
        });

        // const info = ["/**\n"];
        const info = [];

        for (const transform of this.transforms) {
            console.log("=================================");
            const transform_name = transform.constructor.name;
            const dependencies = transform.get_dependencies();
            for (const dependency of dependencies) {
                if (!this.run_transforms.includes(dependency)) {
                    throw `Transform '${transform_name}' had unmet dependency '${dependency}'`;
                }
            }
            const transform_description = transform.get_description();
            console.log("Running transformer: " + transform_description);
            info.push(`console.log("${transform_description}");\n`);
            transform.reset();
            transform.apply(this.active_file);
            console.log("");
        }
        // info.push(" */\n");

        let out = generate(this.active_file, {
            sourceFileName: input_file,
            filename: output_file,
        }).code;

        for (const [regex, replacement] of this.replacements) {
            out = out.replace(regex, replacement);
        }

        fs.writeFileSync(output_file, info.join("") + out);

        this.active_file = null;
    }
}

