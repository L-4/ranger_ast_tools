import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

export class RemoveIncrements extends ITransform {
    public get_description(): string {
        return "Remove ++ and -- postfixes";
    }

    private static readonly update_to_assign = { "++": "+=", "--": "-=" };

    public constructor() {
        super();
        this.traverse_options = {
            // ExpressionStatement: path => {
            //     const expression = path.node.expression;
            //     if (expression.type === "UpdateExpression" && !expression.prefix) {
            //         path.replaceWith({
            //             type: "ExpressionStatement",
            //             // @ts-ignore
            //             expression: {
            //                 type: "AssignmentExpression",
            //                 left: expression.argument,
            //                 operator: RemoveIncrements.update_to_assign[expression.operator],
            //                 right: {
            //                     type: "NumericLiteral",
            //                     value: 1,
            //                 },
            //             },
            //         })
            //     }
            // },
            UpdateExpression: path => {
                const parent = path.parent;
                if (t.isExpressionStatement(parent)
                 || t.isForStatement(parent)) {
                    const node = path.node;
                    path.replaceWith({
                        type: "AssignmentExpression",
                        // @ts-ignore
                        left: node.argument,
                        operator: RemoveIncrements.update_to_assign[node.operator],
                        right: {
                            type: "NumericLiteral",
                            value: 1,
                        },
                    });
                }
            },
        };
    }
}
