import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class SomeTransform extends ITransform {
    public get_description(): string {
        return "Transform with no description";
    }

    public constructor() {
        super();
        this.traverse_options = {};
    }
}
