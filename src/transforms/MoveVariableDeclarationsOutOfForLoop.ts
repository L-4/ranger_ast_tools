import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

// @todo: I don't know that this is a good idea. It should be valid as long as
// we always use `var`, right?
/**
 * Converts code like
 * ```
 * for (var i, j, k = 0; i < 10; i++) {}
 * ```
 *
 * to
 * ```
 * var i,
 *     j,
 *     k = 0;
 *
 * for (; i < 10; i++) {}
 * ```
 *
 * If only one variable is declared in the init, then we keep it.
 *
 * @todo If we have multiple declarations, we should try to see if we're a "normal"
 * for loop (ie something like `for (var i, j = 0; i < 10; i++)`).
 * If we are, then we should try to leave the common variable declaration in the
 * init statement.
 */
export class MoveVariableDeclarationsOutOfForLoop extends ITransform {
    public get_description(): string {
        return "Move variable declarations in the for loop init expressions out.";
    }

    public constructor() {
        super();
        this.traverse_options = {
            VariableDeclaration: path => {
                // Even if we're in a for loop, if we only have a single declaration then it's better to leave it in.
                if (path.node.declarations.length == 1) return;
                const parent_node = path.parentPath.node;
                if (t.isForStatement(parent_node)) {
                    // @ts-ignore :???:
                    if (path.node === parent_node.init) { // @todo: loc_equals?
                        path.parentPath.insertBefore(parent_node.init);
                        parent_node.init = null;
                    } else {
                        throw fmt_loc(parent_node.loc, "Uh oh- variable declaration in test or update expression? (?????)");
                    }
                }
            },
        };
    }
}
