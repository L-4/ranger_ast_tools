// @ts-nocheck
import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { err_loc, expect_type, loc } from "../utils";

export class RemoveCommaOperatorFromLoops extends ITransform {
    public get_description(): string {
        return "Remove comma operators from for loops";
    }

    public constructor() {
        super();
        this.traverse_options = {
            ForStatement: (path) => {
                const node = path.node;

                // expect_type(path, node.init, ["AssignmentExpression", "SequenceExpression", "VariableDeclaration"], "In node.init");

                if (node.init !== null && node.init.type == "SequenceExpression") {
                    const init = node.init;
                    if (init.expressions.length <= 1) {
                        throw "Rock";
                    }
                    // We have to move out the first expressions as to keep the evaluation order
                    const extra_expressions = init.expressions.slice(0, -1);
                    for (let i = 0; i < extra_expressions.length; ++i) {
                        const old_expression = extra_expressions[i];
                        extra_expressions[i] = {
                            type: "ExpressionStatement",
                            expression: old_expression,
                        };
                    }
                    const last_expression = init.expressions[init.expressions.length - 1];
                    node.init = last_expression;

                    path.insertBefore(extra_expressions);
                }

                if (node.test != null) {
                    expect_type(node.test, ["BinaryExpression", "LogicalExpression"], "For loop was not one of [$EXPECTED_TYPE], got $GOT_TYPE")
                }

                if (node.update !== null && node.update.type === "SequenceExpression") {
                    const update = node.update;
                    if (update.expressions.length <= 1) {
                        throw "Rock";
                    }
                    // We have to move out the first expressions as to keep the evaluation order
                    const extra_expressions = update.expressions.slice(0, -1);
                    for (let i = 0; i < extra_expressions.length; ++i) {
                        const old_expression = extra_expressions[i];
                        extra_expressions[i] = {
                            type: "ExpressionStatement",
                            expression: old_expression,
                        };
                    }
                    const last_expression = update.expressions[update.expressions.length - 1];

                    node.update = last_expression;
                    node.body.body = node.body.body.concat(extra_expressions);
                }
            }
        };
    }
}
