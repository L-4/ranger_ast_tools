import * as t from "@babel/types";
import { ITransform } from "../ITransform";

export class ConvertFunctionPrototypes extends ITransform {
    public get_description(): string {
        return "Transform with no description";
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: path => {
                const expression = path.node.expression;
                if (!t.isAssignmentExpression(expression)) {
                    return;
                }

                if (expression.operator !== "=") {
                    return;
                }

                const left = expression.left;
                const right = expression.right;

                if (!t.isFunctionExpression(right)) {
                    return;
                }
            },
        };
    }
}
