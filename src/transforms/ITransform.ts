import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import traverse from "@babel/traverse";

export abstract class ITransform {
    public get_description(): string {
        return "Transform with no description";
    }

    public get_dependencies(): string[] {
        return [];
    }

    protected traverse_options: TraverseOptions;

    public reset(): void {}

    public apply(root: Node): void {
        traverse(root, this.traverse_options);
    }
}
