import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { node_default } from "../utils";

export class FixBooleanShorthands extends ITransform {
    public get_description(): string {
        return "Convert !0 to true and !1 to false";
    }

    public constructor() {
        super();
        this.traverse_options = {
            UnaryExpression: (path) => {
                const node = path.node;
                if (node.operator !== "!") {
                    return;
                }

                if (node.argument.type !== "NumericLiteral") {
                    return;
                }

                const new_literal = {
                    ...node_default,
                    type: "BooleanLiteral",
                    value: node.argument.value === 0,
                }
                // @ts-ignore
                path.replaceWith(new_literal);
            },
        };
    }
}
