import * as t from "@babel/types";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { NodePath } from "@babel/traverse";
import { loc_equals } from "../utils";

const is_identifier_renamable = (path: NodePath<t.Identifier>): boolean => {
    const parent = path.parent;

    if (t.isMemberExpression(parent)) {
        // If parent is MemberExpression, then we only want to rename if the identifier
        // is the object, not the property.

        // We can do other checks, but really we just care that we are the parents object
        if (!loc_equals(path.node.loc, parent.object.loc)) {
            if (parent.computed) {
                if (!loc_equals(path.node.loc, parent.property.loc)) {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    // True by default. Hope that works out.
    return true;
}

export type GlobalRenames = { [K: string]: string }

/**
 * Renames global symbols. This is easy since globals seem to be guaranteed to
 * never be shadowed within a scope.
 * We simply find all identifiers, and rename them if they are an actual symbol.
 */
export class RenameGlobals extends ITransform {
    public get_description(): string {
        return "Renames globals (functions and variables)";
    }

    public constructor(renames: GlobalRenames) {
        super();
        this.traverse_options = {
            Identifier: path => {
                if (!is_identifier_renamable(path)) return;
                const node = path.node;

                if (renames[node.name] != undefined) {
                    node.name = renames[node.name];
                }
            },
        };
    }
}

export type LocalRenames = { [K: string]: { [K: string]: string } }

/**
 * Renames local symbols, including function parameters and locals.
 * This works the same as RenameGlobals, except it works within individual scopes.
 * Note that while you can rename a local variable within a function, variables
 * are sometimes reused with the same name.
 */
export class RenameLocals extends ITransform {
    public get_description(): string {
        return "Renames globals (functions and variables)";
    }

    public constructor(local_renames: LocalRenames) {
        super();
        this.traverse_options = {
            // Note: function assignments (like Vector2.prototype.set) are parts of expression statements,
            // not function declarations.
            FunctionDeclaration: path => {
                const node = path.node;
                const renames = local_renames[node.id.name];

                if (renames == undefined) return;

                traverse(node, {
                    Identifier: path => {
                        if (!is_identifier_renamable(path)) return;
                        const node = path.node;

                        if (renames[node.name] != undefined) {
                            node.name = renames[node.name];
                        }
                    },
                }, path.scope, undefined, path);
            }
        };
    }
}

/**
 * Renames identifiers that start with characters not allowed in other languages.
 * Only useful for porting.
 */
export class RenameInvalidIdentifiers extends ITransform {
    public get_description(): string {
        return "Renames invalid identifiers like $";
    }

    public constructor() {
        super();
        this.traverse_options = {
            Identifier: path => {
                if (!is_identifier_renamable(path)) return;

                const node = path.node;
                if (node.name.includes("$")) {
                    path.replaceWith({
                        ...node,
                        // @ts-ignore
                        name: node.name.replace("$", "_ds_"),
                    })
                }
            },
        };
    }
}
