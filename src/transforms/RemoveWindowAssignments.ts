import * as t from "@babel/types";
import { ITransform } from "./ITransform";

/**
 * Removes instances where functions are assigned to window.fff.
 * I'm not sure why this was ever emitted - maybe to circumvent them being
 * optimized out somehow?
 */
export class RemoveWindowAssignments extends ITransform {
    public get_description(): string {
        return "Remove all instances of <window alias>.fff = whatever";
    }

    private static readonly WINDOW_GLOBAL_NAME = "window";
    private static readonly WINDOW_PROPERTY_NAME = "fff";

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: path => {
                const node = path.node.expression;
                if (!t.isAssignmentExpression(node)) return;

                const left = node.left;
                if (!t.isMemberExpression(left)) return;

                const object = left.object;
                const property = left.property;
                if (!t.isIdentifier(object)) return;
                if (!t.isIdentifier(property)) return;

                if (object.name == RemoveWindowAssignments.WINDOW_GLOBAL_NAME &&
                    property.name == RemoveWindowAssignments.WINDOW_PROPERTY_NAME) {
                    path.remove()
                }
            },
        };
    }
}
