import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { separate_if_sequence } from "../utils";

export class RemoveCommaOperatorFromExpressions extends ITransform {
    public get_description(): string {
        return "Remove comma operators from expression statements";
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: (path) => {
                if (path.node.expression.type === "SequenceExpression") {
                    // @ts-ignore
                    path.replaceWithMultiple(separate_if_sequence(path.node.expression));
                }
            },
        };
    }
}
