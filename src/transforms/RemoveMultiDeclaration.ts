import * as t from "@babel/types";
import { Node } from "@babel/types";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";
import { TraverseOptions } from "babel-traverse";
import { loc } from "../utils";

// @todo: If there's a declaration without any expression (say `var a, b ,c;`)
// Maybe it's best to leave it alone?
export class RemoveMultiDeclaration extends ITransform {
    public get_description(): string {
        return "Split multiple variable declarations with one `var` into multiple";
    }

    private static readonly valid_split_contexts = ["BlockStatement", "Program"];

    public constructor() {
        super();
        this.traverse_options = {
            VariableDeclaration: (path) => {
                if (!RemoveMultiDeclaration.valid_split_contexts.includes(path.parentPath.node.type)) {
                    console.warn(`${loc(path)} Parent was ${path.parentPath.node.type} which may not be safe to split.`);
                    return;
                }

                if (path.node.declarations.length == 1) {
                    return;
                }

                const new_declarations = [];

                for (const declaration of path.node.declarations) {
                    new_declarations.push({
                        type: "VariableDeclaration",
                        kind: path.node.kind,
                        declarations: [declaration],
                    });
                }

                path.replaceWithMultiple(new_declarations);
            },
        };
    }
}
