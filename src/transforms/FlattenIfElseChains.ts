import * as t from "@babel/types";
import { Node } from "@babel/types";
import { TraverseOptions } from "babel-traverse";
import { ITransform } from "./ITransform";
import traverse from "@babel/traverse";

export class FlattenIfElseChains extends ITransform {
    public get_description(): string {
        return "Flatten if/else chains";
    }

    public constructor() {
        super();
        this.traverse_options = {
            IfStatement: (path) => {
                const alternate = path.node.alternate;
                if (alternate === null) {
                    return;
                }

                if (alternate.type !== "BlockStatement") {
                    return;
                }

                if (alternate.body.length === 1 && alternate.body[0].type === "IfStatement") {
                    path.replaceWith({
                        type: "IfStatement",
                        // @ts-ignore
                        test: path.node.test,
                        consequent: path.node.consequent,
                        alternate: alternate.body[0],
                    })
                }
            },
        };
    }
}
