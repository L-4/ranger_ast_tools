import * as t from "@babel/types";
import { ITransform } from "./ITransform";

export class RemoveEmptyLiterals extends ITransform {
    public get_description(): string {
        return "Removes empty literals at the file scope";
    }

    public constructor() {
        super();
        this.traverse_options = {
            ExpressionStatement: path => {
                const node = path.node;
                if (t.isStringLiteral(node.expression)) {
                    path.remove();
                }
            }
        };
    }
}
