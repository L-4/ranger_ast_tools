import { TransformRunner } from "./transforms/TransformRunner";
import { AddMissingBlocks, FixBooleanShorthands, RemoveCommaOperatorFromIfStatement, RemoveCommaOperatorFromLoops, RemoveCommaOperatorFromExpressions, RemoveMultiDeclaration, SeparateMultipleAssignments, ConvertShortCircuitTernaryLogic, SwapOperatorSides, FlattenIfElseChains, TestTransform, MoveVariableDeclarationsOutOfForLoop, RemoveIncrements, AddBlockIndicators, RenameInvalidIdentifiers, InsertReflectionCalls, ChainedIfToSwitch, RemoveWindowAssignments, RenameGlobals, RenameLocals, GlobalRenames, LocalRenames } from "./transforms";
import { generate_odin } from "./generator/generate_odin";
import * as fs from "fs";

// The strategy:
// 1. Parse *original* source code, and beautify it. It should be as close to valid
//      Odin code as possible, while still able to run as JS.
// 2. Modify emitter to get just a bit closer to Odin source. At this point it will
//      be neither valid JS code nor Odin code.
// 3. Incrementally port code to Odin.

// Issues remaining for 1.
// - Eliminate all instances of ++ and -- where possible.
// - Remove $ from all variable names
// - Add parenthesis to all binary operators (very important)
// And maybe:
// - We have cases like `if (Ab) {}`. These are unlikely to work. Automatically insert
//      if (truthy(Ab)) maybe?

// Issues remaining for 2.
// - Guesses for types have to be made.
//      This could either be done only for values which we are sure of the type for,
//      or try to apply a best guess. Runtime info will be required for this.
// - Change a.length to len(a)?
// And maybe:
// - Parenthesis in for loops in odin are not allowed. Can we modify the emitter to exclude them?
//      This can be trivially solved with this regex however:
//      for \(((.*;.*){2})\) \{
//      for $1 {
// - Try wrapping every statement in block comments. Makes it easier to incrementally uncomment.
//      This can be trivially solved with these regexes:
//      /\*__BEGIN_STATEMENT__\*/\n\s*
//      /*
//      \n\s*/\*__END_STATEMENT__\*/\n
//       */
// - Are there instances where we can insert info about stuff which will be helpful in Odin?
//      - fallthrough in cases without break.

// Very nice to have:
// - Can we tell where variables are declared? Doing so would speed up porting a lot.

// All invalid syntax from the top to bottom:
// Using var: We can use var to guess where a variable is declared. Not difficult to remove however.
// Usage of window: We need to remember where window is used so that we can substitute, but otherwise
//    it can simply be removed.
// Uninitialized variable: We need to give these a type. As we're using the original variable names,
//    we can hopefully lazily type them when we need them, using the JS version for reference.
// New object: Hopefully we can figure this out lazily. We only know what something is by how it is used.
// Array literals, and Array(): We can lazily figure out what they might contain later.
// "asd".split(): This can be constant evaluated.
// Large arrays of data: These can hopefully be copied from an earlier port
// Assigning to window: Most of the time, these should be able to be removed entirely.
// .length: Hopefully these can be swapeed for len()
// Lack of return type: Hopefully can be inferred from function. Luckily only has one per function.

// For emitter: We don't write function literals

/*
const USAGE_INT       = 1 << 0;
const USAGE_FLOAT     = 1 << 1;
const USAGE_STRING    = 1 << 2;
const USAGE_ARRAY     = 1 << 3;
const USAGE_OBJECT    = 1 << 4;
const USAGE_F32_ARRAY = 1 << 5;
const USAGE_I32_ARRAY = 1 << 6;
*/

type AllRenames = {
	global_symbols: GlobalRenames,
	local_symbols: LocalRenames,
}

const ranger1_renames = JSON.parse(fs.readFileSync("ranger1_renames.json").toString()) as AllRenames;

const transform_runner_all = new TransformRunner([
//   new ChainedIfToSwitch(),
  new RenameGlobals(ranger1_renames.global_symbols),
  new RenameLocals(ranger1_renames.local_symbols),
//   new RenameInvalidIdentifiers(), // Only useful when porting
//   new ConvertTruncationOperator(),
  new ConvertCharCodeLiterals(),
  new RemoveEmptyLiterals(),
  new RemoveWindowAssignments(),
  new MoveVariableDeclarationsOutOfForLoop(),
  new AddMissingBlocks(),
  new SwapOperatorSides(),
  new RemoveMultiDeclaration(),
  new FixBooleanShorthands(),
  new RemoveCommaOperatorFromIfStatement(),
  new RemoveCommaOperatorFromLoops(),
  new RemoveCommaOperatorFromExpressions(),
  new ConvertShortCircuitTernaryLogic(),
  new SeparateMultipleAssignments(),
  new FlattenIfElseChains(),
// SPECIAL TRANSFORMS
//   new TestTransform(),
//   new RemoveIncrements(),
  // new AddBlockIndicators(),
  // new InsertReflectionCalls(),
], [
  // [/for \(((.*;.*){2})\) \{/g, "for $1 {"],
  // [/\/\*__BEGIN_STATEMENT__\*\/\n\s*/g, "/* "],
  // [/\n\s*\/\*__END_STATEMENT__\*\/\n/g, " */"],
  // [/Math\.(cos|sin|random|sqrt|floor)/g, "Math_$1"],
]);//

// const [in_file, out_file] = ["input/12_9/raw.js", "output/ranger2.js"];
// const [in_file, out_file] = ["input/12_9/rawb.js", "output/ranger2.js"];

// const [in_file, out_file] = ["input/compass.js", "output/compass.js"];

// const [in_file, out_file] = ["input/12_8/raw.js", "output/ranger2_12_8_deob.js"];
// const [in_file, out_file] = ["input/11_6/raw.js", "output/rename_test/ranger2_11_6.js"];
// const [in_file, out_file] = ["input/12_8/raw.js", "output/rename_test/ranger2_12_8.js"];

{
//   const [in_file, out_file] = ["input/test_in.js", "output/test_out.js"];
  const [in_file, out_file] = ["input/ranger1/ranger.js", "output/ranger.js"];
//   const [in_file, out_file] = ["input/ranger1/rangerb.js", "output/ranger.js"];
//   const [in_file, out_file] = ["input/compass.js", "compass/compass.js"];

  transform_runner_all.run(in_file, out_file);
//   generate_odin({
//     input_file: out_file,
//     output_file: "compass/compass.odin",
//   });
      }
