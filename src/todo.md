## Fix short circuit ifs. Examples:
```js
a == ic && d++;
// Becomes
if (a == ic) {
    d++;
}
```

```js
Oe(p) && (p == 1 && (rc >>= 1), ua = 0, ta++);
// Becomes
if (Oe(p)) {
    if (p == 1) {
        rc >>= 1;
    }
    ua = 0;
    ta++;
}
```

- An expression statement, which is to say an expression where the computed value goes unused,
    where the operator is `&&`, seems to imply a missing if.
- Short circuiting given `&&` can only happen with two statements. For nested ifs to be possible,
    a sequence expression is required.
- A case like this `a && b++` is simple. It can be translated to a single if. However, a case only
    barely more complicated, `a && b && c++`, can either by parsed as `if (a && b) { c++ }` or `if (a) { if (b) { c++ } }`.
    Both are valid, but it is neither obvious which one the programmer originally meant, nor which one
    would be more readable from the deobfuscator.

This makes it easy to generalize the short-circuiting behavior as this:
<condition> && <expression> -> if (<condition>) { <expression> }
<condition> || <expression> -> if (!<condition>) { <expression> }

 - This can only be applied to (and would only be useful in) cases where an if statement is legal.
    This is to say that if the obfuscated code looks like `if (a && b && c++) {}`, that it would
    be both more difficult, but also less likely to revert the code to any semblance of the original.


```js
b < 0 || b >= 432 || (jh[b] > f && (jh[b] = f), kh[b] < f && (kh[b] = f));
// Becomes
if (!(b < 0 || b >= 432)) {
    if (jh[b] > f) {
        jh[b] = f;
    }

    if (kh[b] < f) {
        kh[b] = f;
    }
}
// Or
if (b < 0) {

} else if (b >= 432) {

} else {
    if (jh[b] > f) {
        jh[b] = f;
    }

    if (kh[b] < f) {
        kh[b] = f;
    }
}
// Or
```
## In multiple assignment before a for loop, choose right variable for initialization.
```js
for (a = Ma = La = 0; 4 > a; a++) Na[a] = 0, Oa[a] = 50, Pa[a] = 50, Qa[a] = 0;
// Gets translated to
a = 0
Ma = 0
for (La = 0; a < 4; a++) {
    Na[a] = 0
    Oa[a] = 50
    Pa[a] = 50
    Qa[a] = 0
}
```
This is nice in that it keeps the order of the variables as they're declared, but in this case,
`a` should be chosen, either due to the fact that it is obvious that it was intended from the context,
or that this generator seems to put it as the first variable.

## Temp
`if (vg[L] != kg && (k = O[L][2], !(k.x > c || k.x < f || k.y > d || k.y < h))) {`

## Ideas
 - Move var declarations to first tile that it was assigned to, given that it is not further down in the scope?
 - In the function `Jg`, there is this line
```js
return Sf(a, b, 24, 24) ? (ei(ia, a, b - 3, 16, 16, c * 16, 24, 16, 16, 16737894), d.length >= 6 ? Fm(a, b + 8, d, 16737894) : blit_string_centered(Bf, a, b + 8, d, 16737894, -1), true) : false;
```
This should be converted to an if/else, due to it having a sequence.


## Function 'Mf' Still uses ~~. Look into

## Constant evaluate string.fromcharcode etc
