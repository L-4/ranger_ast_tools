import * as fs from "fs";
import { parse } from "@babel/parser";
import * as t from "@babel/types";

const floc = (c: Context, n: t.Node, m: string = "") => {
    throw `${m}: ./${c.options.input_file}:${n.loc.start.line}:${n.loc.start.column}`;
}

type GeneratorOptionsRequired = {
    input_file: string,
    output_file: string,
};

type GeneratorOptionsOptional = {
    package_name: string,
};

const optional_defaults: GeneratorOptionsOptional = {
    package_name: "db_wrap",
};

type GeneratorOptions = GeneratorOptionsRequired & GeneratorOptionsOptional;

type Context = {
    source: string[],
    indentation: number,
    options: Readonly<GeneratorOptions>,
    in_global_block: boolean,
    global_block_idx: number,
};

const write_line = (ctx: Context, line_contents: string) => {
    ctx.source.push("\t".repeat(ctx.indentation) + line_contents + "\n");
};

const write_empty_line = (ctx: Context) => {
    ctx.source.push("\n");
};

const try_enter_global_block = (ctx: Context) => {
    if (ctx.in_global_block) return;
    if (ctx.indentation > 0) return;

    write_line(ctx, `@(init) global_block_${ctx.global_block_idx} :: proc() {`);
    ctx.in_global_block = true;
    ctx.indentation++;
};

const try_exit_global_block = (ctx: Context) => {
    if (!ctx.in_global_block) return;
    if (ctx.indentation !== 1) return;

    ctx.in_global_block = false;
    ctx.indentation--;
    ctx.global_block_idx++;
    write_line(ctx, "}");
};

const does_member_expression_match = (expression: t.Expression, ...strings: string[]): boolean => {
    let str_idx = 0;
    strings = strings.reverse();

    while (true) {
        if (str_idx === strings.length - 1) {
            // On the final string, we're just looking for an identifier.
            if (!t.isIdentifier(expression)) return false;
            return expression.name === strings[str_idx];
        } else {
            if (!t.isMemberExpression(expression)) return false;
            if (!t.isIdentifier(expression.property)) return false;
            if (expression.property.name !== strings[str_idx]) return false;

            expression = expression.object;
            str_idx++;
        }
    }
};

const USAGE_UNKNOWN = 0;
const USAGE_INT = 1 << 0;
const USAGE_FLOAT = 1 << 1;
const USAGE_STRING = 1 << 2;
const USAGE_BOOLEAN = 1 << 3
const USAGE_ARRAY = 1 << 4;
const USAGE_OBJECT = 1 << 5;
const USAGE_F32_ARRAY = 1 << 6;
const USAGE_I32_ARRAY = 1 << 7;
const USAGE_U32_ARRAY = 1 << 8;

const odin_type_names = [
    "int", "f32", "string", "bool", "Array", "Object", "F32Array", "I32Array", "U32Array",
];

const value_types: { [K: string]: number } = { "g": 1, "l": 1, "p": 1, "aa": 32, "ba": 0, "q": 1, "r": 1, "ca": 1, "t": 1, "da": 1, "v": 8, "x": 1, "G": 16, "H": 1, "F": 32, "y": 32, "za": 32, "Aa": 32, "Ba": 256, "Ca": 32, "J": 0, "sa": 0, "Da": 4, "ga": 4, "na": 4, "Ga": 4, "qa": 4, "ra": 4, "Ha": 1, "Ia": 4, "E": 16, "K": 128, "L": 128, "Sa": 1, "cb": 1, "bb": 1, "db": 1, "X": 2, "eb": 2, "fb": 2, "gb": 2, "ma": 1, "ib": 16, "jb": 16, "la": 32, "U": 1, "Ja": 8, "Ka": 8, "M": 8, "N": 8, "La": 8, "Na": 8, "Ma": 8, "O": 8, "Oa": 8, "P": 1, "Pa": 1, "Q": 1, "S": 1, "R": 1, "T": 1, "Y": 1, "Qa": 3, "Ra": 3, "Va": 8, "ia": 16, "ja": 16, "ka": 16, "A": 16, "B": 16, "Z": 8, "pa": 4, "C": 16, "z": 16, "ha": 2, "Ta": 2, "a": 39, "b": 51, "c": 39, "d": 35, "e": 19, "f": 51, "k": 17, "m": 17, "h": 35, "n": 35, "w": 33, "u": 3, "Fa": 2, "pb": 1, "qb": 1, "rb": 3, "sb": 2, "tb": 1 };
const param_types: { [K: string]: { [K2: string]: number } } = { "fa": { "a": 4 }, "D": { "a": 3 }, "Ua": { "a": 32, "b": 3 }, "Wa": { "a": 3, "b": 3, "c": 3, "d": 2, "e": 3, "f": 2, "k": 1 }, "mb": { "a": 3, "b": 3, "c": 3, "d": 3 }, "lb": { "a": 3 }, "I": { "a": 1, "b": 1, "c": 1, "d": 1, "e": 1 }, "V": { "a": 1, "b": 1, "c": 4, "d": 3, "e": 2 }, "Za": { "a": 1, "b": 1 }, "$a": { "a": 1, "b": 1, "c": 1, "d": 1 }, "Ea": { "a": 32, "b": 1, "c": 1 }, "nb": { "a": 32 }, "W": { "a": 1, "b": 1, "c": 1 }, "ab": { "a": 1, "b": 1 }, "ya": { "a": 3, "b": 1, "c": 3, "d": 1, "e": 1 }, "Xa": { "a": 3, "b": 3, "c": 1, "d": 1 } };

const type_mask_into_type = (types: number): string => {
    for (let i = 0; i < odin_type_names.length; ++i) {
        const mask = 1 << i;
        if (types == mask) { // only one type
            return odin_type_names[i];
        }
    }

    if (types == (USAGE_INT | USAGE_FLOAT)) { // Float which happens to be even sometimes
        return "f32";
    }

    return "Multiple";
}

const FORCE_SINGLE_TYPE = true;
const SINGLE_TYPE = "Obj"

const get_best_type_for = (name: string): string => {
    if (FORCE_SINGLE_TYPE) {
        return SINGLE_TYPE;
    } else {
        const types = value_types[name];
        if (types === undefined) return "Best_Guess_Shouldnt_Happen";

        return type_mask_into_type(types);
    }
};

const get_best_type_for_param = (name: string, func: string): string => {
    if (FORCE_SINGLE_TYPE) {
        return SINGLE_TYPE;
    } else {
        const params = param_types[func];

        if (params === undefined) return "Best_Guess_Shouldnt_Happen";

        const types = params[name];

        if (types === undefined) return "Best_Guess_Shouldnt_Happen";

        return type_mask_into_type(types);
    }
};

const write_statements = (ctx: Context, statements: t.Statement[]) => {
    for (const statement of statements) {
        if (t.isFunctionDeclaration(statement)) {
            try_exit_global_block(ctx);
            write_function_declaration(ctx, statement);
        } else if (t.isIfStatement(statement)) {
            try_enter_global_block(ctx);
            write_if_statement(ctx, statement);
        } else if (t.isExpressionStatement(statement)) {
            try_enter_global_block(ctx);
            write_line(ctx, print_expression(ctx, statement.expression));
        } else if (t.isVariableDeclaration(statement)) {
            try_exit_global_block(ctx);
            write_line(ctx, print_variable_declaration(ctx, statement));
        } else if (t.isReturnStatement(statement)) {
            write_line(ctx, `return ${statement.argument !== null ? print_expression(ctx, statement.argument) : ""}`);
        } else if (t.isBreakStatement(statement)) {
            if (statement.label !== null) throw "Rock";
            write_line(ctx, "break");
        } else if (t.isThrowStatement(statement)) {
            write_line(ctx, `fmt.panic(${print_expression(ctx, statement.argument)})`);
        } else if (t.isForStatement(statement)) {
            try_enter_global_block(ctx);
            write_for_statement(ctx, statement);
        } else if (t.isSwitchStatement(statement)) {
            try_enter_global_block(ctx);
            write_switch_statement(ctx, statement);
        } else if (t.isTryStatement(statement)) {
            try_enter_global_block(ctx);
            write_try_statement(ctx, statement);
        } else if (t.isEmptyStatement(statement)) {
            // Noop
        } else {
            write_line(ctx, `/*unhandled ${statement.type}*/`);
        }
    }
};

const print_variable_declaration = (ctx: Context, variable_declaration: t.VariableDeclaration): string => {
    if (variable_declaration.declarations.length !== 1) {
        console.log(variable_declaration.loc);
        floc(ctx, variable_declaration, "ROck 11");
    }
    const declaration = variable_declaration.declarations[0];
    if (!t.isIdentifier(declaration.id)) {
        throw "Rock 12"; // Required? id is a lvalue
    }

    if (declaration.init !== null) {
        return `${print_expression(ctx, declaration.id)}: Obj = ${print_expression(ctx, declaration.init)}`;
    } else {
        return `${print_expression(ctx, declaration.id)}: ${get_best_type_for(declaration.id.name)}`;
    }
};

const print_expression = (ctx: Context, expression: t.Expression | t.LVal | t.PrivateName): string => {
    if (t.isPrivateName(expression)) {
        throw "PrivateName";
    }

    if (t.isAssignmentExpression(expression)) {
        return `${print_expression(ctx, expression.left)} ${expression.operator} ${print_expression(ctx, expression.right)}`;
    } else if (t.isNumericLiteral(expression) || t.isBooleanLiteral(expression)) {
        return String(expression.value);
    } else if (t.isStringLiteral(expression)) {
        return `"${String(expression.value)}"`;
    } else if (t.isNullLiteral(expression)) {
        return "null";
    } else if (t.isThisExpression(expression)) {
        return "this";
    } else if (t.isIdentifier(expression)) {
        return expression.name;
    } else if (t.isBinaryExpression(expression)) {
        return print_binary_expression(ctx, expression);
    } else if (t.isLogicalExpression(expression)) {
        if (expression.operator === "??") throw "Rock";
        return `(${print_expression(ctx, expression.left)} ${expression.operator} ${print_expression(ctx, expression.right)})`;
    } else if (t.isCallExpression(expression)) {
        return print_call_expression(ctx, expression);
    } else if (t.isNewExpression(expression)) {
        const params = new Array<string>();
        for (const arg of expression.arguments) {
            if (!t.isExpression(arg)) {
                throw "Rock 15";
            }
            params.push(print_expression(ctx, arg));
        }
        if (!t.isExpression(expression.callee)) {
            throw "Rock 16";
        }
        return `new_${print_expression(ctx, expression.callee)}(${params.join(", ")})`;
    } else if (t.isArrayExpression(expression)) {
        const literals = [];

        for (const elem of expression.elements) {
            if (t.isSpreadElement(elem)) throw "Rock and stone";

            literals.push(print_expression(ctx, elem));
        }

        return `0 /* array_lit [${literals.join(", ")}] */`;

    } else if (t.isUpdateExpression(expression)) {
        let first_string = expression.prefix ? "pre" : "post";
        let second_string = expression.operator == "++" ? "inc" : "dec";

        const argument = expression.argument;
        if (!t.isIdentifier(argument) && !t.isMemberExpression(argument)) throw `What the dog doin'? (${argument.type})`;
        return `${first_string}_${second_string}(&${print_expression(ctx, argument)})`;
    } else if (t.isUnaryExpression(expression)) {
        return print_unary_expression(ctx, expression);
    } else if (t.isMemberExpression(expression)) {
        return print_member_expression(ctx, expression);
    } else if (t.isConditionalExpression(expression)) {
        // return print_expression(ctx, expression.test) + " ? " +
        //     print_expression(ctx, expression.consequent) + " : " +
        //     print_expression(ctx, expression.alternate);
        return print_expression(ctx, expression.consequent) + " if " +
            print_expression(ctx, expression.test) + " else " +
            print_expression(ctx, expression.alternate);
    } else {
        return `/*unhandled ${expression.type}*/ 0`;
    }
};

// UNARY/BINARY EXPRESSION
type Op = { op: string };
const unary_operator_lut: { [K: string]: Op | undefined } = {
    "!": { op: "neq" },
    "-": { op: "neg" },
    "delete": { op: "delete" },
    "typeof": { op: "typeof" }, // @todo: Should be handled separately.
};

const print_unary_expression = (ctx: Context, expression: t.UnaryExpression): string => {
    if (expression.operator == "~" && t.isUnaryExpression(expression.argument) && expression.argument.operator == "~") {
        return `op_trunc(${print_expression(ctx, expression.argument.argument)})`;
    }

    const op = unary_operator_lut[expression.operator];

    if (op === undefined) {
        throw `Unhandled unary operator ${expression.operator}`;
    }

    if (!expression.prefix) {
        throw `Expected op_${op.op} to be prefix`;
    }

    return `op_${op.op}(${print_expression(ctx, expression.argument)})`;
}

const binary_operator_lut: { [K: string]: Op | undefined } = {
    "+": { op: "add" },
    "-": { op: "sub" },
    "/": { op: "div" },
    "%": { op: "mod" },
    "*": { op: "mul" },
    "**": { op: "exp" },
    "&": { op: "band" },
    "|": { op: "bor" },
    ">>": { op: "rshift" },
    ">>>": { op: "rshiftz" },
    "<<": { op: "lshift" },
    "^": { op: "xor" },
    "==": { op: "eq" },
    "===": { op: "eq3" },
    "!=": { op: "neq" },
    "!==": { op: "neq3" },
    // "in": { op: "in" },
    // "instanceof": { op: "instanceof" },
    ">": { op: "gt" },
    "<": { op: "lt" },
    ">=": { op: "gte" },
    "<=": { op: "lte" },
};

const print_binary_expression = (ctx: Context, expression: t.BinaryExpression): string => {
    const op = binary_operator_lut[expression.operator];

    if (op === undefined) {
        throw `Unhandled binary operator ${expression.operator}`;
    }

    return `op_${op.op}(${print_expression(ctx, expression.left)}, ${print_expression(ctx, expression.right)})`;
};

// CALL EXPRESSION
const format_arugments = (ctx: Context, args: t.CallExpression["arguments"]): string => {
    const params = new Array<string>();
    for (const arg of args) {
        if (!t.isExpression(arg)) {
            throw "Expected all arguments to be expression";
        }
        params.push(print_expression(ctx, arg));
    }

    return params.join(", ");
}

const print_call_expression = (ctx: Context, expression: t.CallExpression): string => {
    if (t.isV8IntrinsicIdentifier(expression.callee)) throw "Unreachable";
    const args = format_arugments(ctx, expression.arguments);

    if (does_member_expression_match(expression.callee, "Math", "sin")) return `math_sin(${args})`;
    if (does_member_expression_match(expression.callee, "Math", "cos")) return `math_cos(${args})`;
    if (does_member_expression_match(expression.callee, "Math", "random")) return `math_random(${args})`;
    if (does_member_expression_match(expression.callee, "Math", "floor")) return `math_floor(${args})`;

    if (!t.isExpression(expression.callee)) {
        throw "Expected callee to be expression";
    }

    return `${print_expression(ctx, expression.callee)}(${args})`;
};

// MEMBER EXPRESSION
const print_member_expression = (ctx: Context, expression: t.MemberExpression): string => {
    // We don't actually care if member expressions are computed. They'll all need to go through a proxy function anyway
    return `op_index(${print_expression(ctx, expression.object)}, ${print_expression(ctx, expression.property)})`;
};

const write_for_statement = (ctx: Context, statement: t.ForStatement) => {
    let init = "";
    if (statement.init !== null) {
        if (t.isVariableDeclaration(statement.init)) {
            init = print_variable_declaration(ctx, statement.init);
        } else {
            init = print_expression(ctx, statement.init);
        }
    }

    // Extra space for beauty factor.
    let test = statement.test !== null ? " " + print_expression(ctx, statement.test) : "";
    let update = statement.update !== null ? " " + print_expression(ctx, statement.update) : "";

    write_line(ctx, `for ${init};${test};${update} {`);
    ctx.indentation++;
    if (!t.isBlockStatement(statement.body)) throw "Rock 18";
    write_statements(ctx, statement.body.body);
    ctx.indentation--;
    write_line(ctx, "}");
}

const write_function_declaration = (ctx: Context, declaration: t.FunctionDeclaration) => {
    if (declaration.id === null) throw "Rock 2";
    const param_strings = new Array<string>();

    for (const param of declaration.params) {
        if (t.isIdentifier(param)) {
            param_strings.push(`${param.name}: ${get_best_type_for_param(param.name, declaration.id.name)}`);
        } else {
            throw "Rock 3";
        }
    }

    write_line(ctx, `${declaration.id.name} :: proc(${param_strings.join(", ")}) {`);
    ctx.indentation++;
    write_statements(ctx, declaration.body.body);
    ctx.indentation--;
    write_line(ctx, "}");
}

const write_if_statement = (ctx: Context, statement: t.IfStatement) => {
    write_line(ctx, `if truthy(${print_expression(ctx, statement.test)}) {`);
    ctx.indentation++;
    if (!t.isBlockStatement(statement.consequent)) throw "Rocljhkl";
    write_statements(ctx, statement.consequent.body);
    ctx.indentation--;

    let alternate = statement.alternate;

    while (alternate !== null && t.isIfStatement(alternate)) {
        write_line(ctx, `} else if truthy(${print_expression(ctx, statement.test)}) {`);
        ctx.indentation++;
        if (!t.isBlockStatement(alternate.consequent)) throw "Rockasd";
        write_statements(ctx, alternate.consequent.body);
        ctx.indentation--;

        alternate = alternate.alternate;
    }

    if (alternate !== null) {
        write_line(ctx, "} else {");
        ctx.indentation++;
        if (!t.isBlockStatement(alternate)) throw "Rockggg";
        write_statements(ctx, alternate.body);
        ctx.indentation--;
    }

    write_line(ctx, "}");
};

const write_switch_statement = (ctx: Context, statement: t.SwitchStatement) => {
    write_line(ctx, `switch ${print_expression(ctx, statement.discriminant)} {`)
    ctx.indentation++;

    for (const case_ of statement.cases) {
        write_line(ctx, `case ${print_expression(ctx, case_.test)}: {`);
        ctx.indentation++;
        write_statements(ctx, case_.consequent)
        // Default JS behavior is to always fallthrough
        write_line(ctx, "fallthrough")
        //     if (case_.consequent.length == 0) {
        // } else {
        // }
        ctx.indentation--;
        write_line(ctx, "}")
    }
    ctx.indentation--;
    write_line(ctx, "}")
}

const write_try_statement = (ctx: Context, statement: t.TryStatement) => {
    write_line(ctx, "/* try */ {");
    ctx.indentation++;
    write_statements(ctx, statement.block.body);
    ctx.indentation--;

    if (statement.handler !== null) {
        write_line(ctx, "} /* catch */ {");
        ctx.indentation++;
        write_statements(ctx, statement.handler.body.body);
        ctx.indentation--;
    }

    if (statement.finalizer !== null) {
        write_line(ctx, "} /* finally */ {");
        ctx.indentation++;
        write_statements(ctx, statement.finalizer.body);
        ctx.indentation--;
    }

    write_line(ctx, "}");
};

export const generate_odin = (_options: GeneratorOptionsRequired & Partial<GeneratorOptionsOptional>) => {
    const options: GeneratorOptions = { ...optional_defaults, ..._options };

    const contents = fs.readFileSync(options.input_file, "utf-8");
    const file = parse(contents);

    const ctx: Context = {
        source: [],
        indentation: 0,
        options: options,
        in_global_block: false,
        global_block_idx: 0,
    };

    write_line(ctx, `package ${options.package_name}`);
    write_empty_line(ctx);

    write_statements(ctx, file.program.body);

    fs.writeFileSync(options.output_file, ctx.source.join(""));
};

// Statement
// BlockStatement | BreakStatement | ContinueStatement | DebuggerStatement | DoWhileStatement | EmptyStatement | ExpressionStatement | ForInStatement | ForStatement | FunctionDeclaration | IfStatement | LabeledStatement | ReturnStatement | SwitchStatement | ThrowStatement | TryStatement | VariableDeclaration | WhileStatement | WithStatement | ClassDeclaration | ExportAllDeclaration | ExportDefaultDeclaration | ExportNamedDeclaration | ForOfStatement | ImportDeclaration | DeclareClass | DeclareFunction | DeclareInterface | DeclareModule | DeclareModuleExports | DeclareTypeAlias | DeclareOpaqueType | DeclareVariable | DeclareExportDeclaration | DeclareExportAllDeclaration | InterfaceDeclaration | OpaqueType | TypeAlias | EnumDeclaration | TSDeclareFunction | TSInterfaceDeclaration | TSTypeAliasDeclaration | TSEnumDeclaration | TSModuleDeclaration | TSImportEqualsDeclaration | TSExportAssignment | TSNamespaceExportDeclaration;

// Expression
// ArrayExpression | AssignmentExpression | BinaryExpression | CallExpression | ConditionalExpression | FunctionExpression | Identifier | StringLiteral | NumericLiteral | NullLiteral | BooleanLiteral | RegExpLiteral | LogicalExpression | MemberExpression | NewExpression | ObjectExpression | SequenceExpression | ParenthesizedExpression | ThisExpression | UnaryExpression | UpdateExpression | ArrowFunctionExpression | ClassExpression | MetaProperty | Super | TaggedTemplateExpression | TemplateLiteral | YieldExpression | AwaitExpression | Import | BigIntLiteral | OptionalMemberExpression | OptionalCallExpression | TypeCastExpression | JSXElement | JSXFragment | BindExpression | DoExpression | RecordExpression | TupleExpression | DecimalLiteral | ModuleExpression | TopicReference | PipelineTopicExpression | PipelineBareFunction | PipelinePrimaryTopicReference | TSAsExpression | TSTypeAssertion | TSNonNullExpression;
