import * as t from "@babel/types";
import { ExpressionStatement, Node } from "@babel/types";

interface BaseNode {
    leadingComments: ReadonlyArray<Comment> | null;
    innerComments: ReadonlyArray<Comment> | null;
    trailingComments: ReadonlyArray<Comment> | null;
    start: number | null;
    end: number | null;
    loc: t.SourceLocation | null;
    type: Node["type"];
}

export const node_default = {
    leadingComments: null,
    innerComments: null,
    trailingComments: null,
    start: null,
    end: null,
    loc: null,
} as BaseNode;

export const loc = (path: any) => `./some_file:${path.node.loc.start.line}:${path.node.loc.start.column}`;
// export const loc = (path: NodePath<Node>) => `./some_file:${path.node.loc.start.line}:${path.node.loc.start.column}`;
// export const loc = (path: NodePath<Node>) => `./${in_file}:${path.node.loc.start.line}:${path.node.loc.start.column}`;


/**
 * Returns array of expressions if node is sequence, else returns array with only node.
 */
export const separate_if_sequence = (node: Node): ExpressionStatement[] => {
    if (!t.isExpression(node)) {
        throw "Rock";
    }

    if (node.type !== "SequenceExpression") {
        return [t.expressionStatement(node)]
    }

    const generated_expressions = new Array<ExpressionStatement>();
    for (const expression of node.expressions) {
        generated_expressions.push(t.expressionStatement(expression));
    }

    return generated_expressions;
};

export const fmt_loc = (loc: t.SourceLocation, message?: string): string => {
	if (message != undefined) {
		// @ts-ignore
		return `./${loc.filename}:${loc.start.line}:${loc.start.column}: ${message}`
	} else {
		// @ts-ignore
		return `./${loc.filename}:${loc.start.line}:${loc.start.column}`
	}
}

export const err_loc = (loc: t.SourceLocation, message?: string) => {
	console.error(fmt_loc(loc, message));
}

export const loc_equals = (a: t.SourceLocation, b: t.SourceLocation): boolean => {
	return a.start.column == b.start.column &&
		a.start.line == b.start.line &&
		a.end.column == b.end.column &&
		a.end.line == b.end.line;
}

type TypeNodeStrings = t.Node["type"]

export const expect_type = (node: t.Node, valid_types: TypeNodeStrings[], message?: string): boolean => {
	if (!valid_types.includes(node.type)) {
		if (message == undefined) {
			err_loc(node.loc, `Node was unexpected type: expected one of '${valid_types.join(", ")}', got ${node.type}`)
		} else {
			const formatted_message = message
				.replace("$EXPECTED_TYPE", valid_types.join(", "))
				.replace("$GOT_TYPE", node.type)
			err_loc(node.loc, formatted_message)
		}

		return false;
	}

	return true;
}
