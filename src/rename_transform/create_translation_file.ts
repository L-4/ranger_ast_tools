import { parse } from "@babel/parser";
import traverse from "@babel/traverse";
import * as fs from "fs";
import { TraverseOptions } from "babel-traverse";

const [in_file, out_file] = ["output/ranger2_12_8_deob.js", "output/ranger2_deob_translation.json"];

const contents = fs.readFileSync(in_file, "utf-8");
const ast = parse(contents);

const renames: { [key: string]: string } = {};

const traverse_options: TraverseOptions = {
    FunctionDeclaration: path => {
        if (path.node.leadingComments !== undefined) {
            for (const comment of path.node.leadingComments) {
                if (comment.value[0] !== "@") {
                    continue;
                }

                const info = JSON.parse(comment.value.substr(1));
                if (path.node.id.name !== info.symbol_from) {
                    renames[info.symbol_from] = path.node.id.name;
                }
            }
        }
    },
};

traverse(ast, traverse_options);

fs.writeFileSync(out_file, JSON.stringify(renames));

