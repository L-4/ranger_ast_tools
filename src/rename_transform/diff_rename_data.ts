import { SimilarityFactor, FunctionSignature, get_function_signature_similarity, read_signature_data, sum_factors } from "./signature_utils";
import * as fs from "fs";

// const [old_file, new_file] = ["output/rename_test/data/11_6.json", "output/rename_test/data/12_8.json"];
const [old_file, new_file] = ["output/rename_test/data/12_8.json", "output/rename_test/data/12_9.json"];

const old_signatures = read_signature_data(old_file);
const new_signatures = read_signature_data(new_file);

const old_renames: {[key: string]: string} = JSON.parse(fs.readFileSync("output/ranger2_deob_translation.json", "utf-8"));

type SimilaritySummary = {
    factors: SimilarityFactor[],
    name: string,
    old_signature: FunctionSignature,
    score: number,
}

type RenameSuggestion = {
    from: string,
    to: string,
    summary: SimilaritySummary,
}

const rename_suggestions = new Array<RenameSuggestion>();

for (const [old_name, old_signature] of Object.entries(old_signatures)) {
    const function_diffs = new Array<SimilaritySummary>();
    for (const [new_name, new_signature] of Object.entries(new_signatures)) {

        const factors = get_function_signature_similarity(old_signature, new_signature);

        function_diffs.push({
            factors: factors,
            name: new_name,
            old_signature: new_signature,
            score: sum_factors(factors),
        });
    }

    function_diffs.sort((a, b) => a.score - b.score);

    const best_guess = function_diffs[0];
    rename_suggestions.push({
        from: old_name,
        to: best_guess.name,
        summary: best_guess,
    });
    if (old_name === "Qh") {
        console.log(`Best guess for ${old_name}: ${best_guess.name} (score of ${best_guess.score})`);
        for (const factor of best_guess.factors) {
            console.log(`\t${factor.name}: ${factor.score}`);
        }
    }
}

// [new_name]: [suggested_old_names]
const renames_to: {[key: string]: string[] } = {};

for (const suggestion of rename_suggestions) {
    if (renames_to[suggestion.to] === undefined) {
        renames_to[suggestion.to] = [suggestion.from];
    } else {
        renames_to[suggestion.to].push(suggestion.from);
    }
}

const final_renames: { [key: string]: string } = {};

for (const [new_name, old_names] of Object.entries(renames_to)) {
    if (old_names.length > 1) {
        console.log(`Multiple functions want to rename to ${new_name}!`);
        for (const a of old_names) {
            console.log(`\t- ${a}`);
        }
    } else {
        const old_name = old_names[0];
        // If we have a rename for our old name
        if (old_renames[old_name] !== undefined) {
            final_renames[new_name] = old_renames[old_name];
        }
    }
}

fs.writeFileSync("output/final_renames.json", JSON.stringify(final_renames));
console.log(final_renames);
