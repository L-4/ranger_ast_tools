import * as fs from "fs";
import { ITransform } from "./transforms/ITransform";
import { node_default } from "./utils";

export class RenameFunctions extends ITransform {
    public get_description(): string {
        return "Use translation and signature file to apply renames";
    }

    private renames: { [key: string]: string };

    public constructor(translation_file: string) {
        super();

        this.renames = JSON.parse(fs.readFileSync("output/final_renames.json", "utf-8"));
        this.traverse_options = {
            FunctionDeclaration: path => {
                const node = path.node;
                const name = node.id.name;
                const leading_comments = node.leadingComments !== undefined ? node.leadingComments : [];

                for (const comment of leading_comments) {
                    if (comment.value[0] === "@") {
                        // We've been here before.
                        return;
                    }
                }

                leading_comments.push({
                    ...node_default,
                    value: "@ " + JSON.stringify({ symbol_from: name }),
                });

                path.replaceWith({
                    ...path.node,
                    leadingComments: leading_comments,
                });

                if (this.renames[name] !== undefined) {
                    path.scope.rename(name, this.renames[name]);
                }
            },
        };
    }
}
