import { parse } from "@babel/parser";
import traverse_f from "@babel/traverse";
import { traverse as traverse_t } from "@babel/types";
const traverse = traverse_f as typeof traverse_t;
import { TraverseOptions } from "babel-traverse";
import * as fs from "fs";
import { FunctionSignature, LiteralCounter, Signature, SignatureType, write_signature_data } from "./signature_utils";

// const [in_file, out_file] = ["input/11_6/raw.js", "output/rename_test/data/11_6.json"];
// const [in_file, out_file] = ["input/12_8/raw.js", "output/rename_test/data/12_8.json"];
const [in_file, out_file] = ["input/12_9/raw.js", "output/rename_test/data/12_9.json"];

const contents = fs.readFileSync(in_file, "utf-8");
const ast = parse(contents);

const declaration_data: { [key: string]: Signature } = {};
const function_signatures: { [key: string]: FunctionSignature } = {};
const traverse_options: TraverseOptions = {
    // VariableDeclaration: path => {

    // },
    FunctionDeclaration: path => {
        const name = path.node.id.name;
        if (function_signatures[name]) {
            throw "Duplicate function declaration";
        }

        const literals: LiteralCounter = { numeric: 0, boolean: 0, string: 0, null: 0 };
        path.traverse({
            Literal: p => {
                switch (p.node.type) {
                    case "NumericLiteral":
                        literals.numeric++;
                        break;

                    case "BooleanLiteral":
                        literals.boolean++;
                        break;

                    case "StringLiteral":
                        literals.string++;
                        break;

                    case "NullLiteral":
                        literals.null++;
                        break;
                }
            },
        });

        function_signatures[name] = {
            type: SignatureType.Function,
            start: path.node.start,
            size: path.node.end - path.node.start,
            parameter_count: path.node.params.length,
            literals: literals,
        };
    },
};

// @ts-ignore
traverse(ast, traverse_options);

write_signature_data(out_file, function_signatures);
