import * as fs from "fs";

export const enum SignatureType {
    Variable,
    Function,
}

export interface BaseSignature {
    start: number,
}

export interface VariableSignature extends BaseSignature {
    type: SignatureType.Variable,
}

export type LiteralCounter = { numeric: number, boolean: number, string: number, null: number };

export interface FunctionSignature extends BaseSignature {
    type: SignatureType.Function,
    size: number,
    parameter_count: number,
    literals: LiteralCounter,
}

const base_signature_weights = {
    start: 0.0,
};

export type SimilarityFactor = { score: number, name: string };

const get_base_signature_similarity = (a: BaseSignature, b: BaseSignature): SimilarityFactor[] => {
    return [
        { score: Math.abs(a.start - b.start) * base_signature_weights.start, name: "Start pos" },
    ];
}

const function_signature_weights = {
    size_log2: 20,
    parameter_count: 4,
    literals: .1,
};


export const get_function_signature_similarity = (a: FunctionSignature, b: FunctionSignature): SimilarityFactor[] => {
    const factors = get_base_signature_similarity(a, b);

    factors.push({ score: Math.abs(Math.log2(a.size) - Math.log2(b.size)) * function_signature_weights.size_log2, name: "Size log2" });
    factors.push({ score: Math.abs(a.parameter_count - b.parameter_count) * function_signature_weights.parameter_count, name: "Param count" });
    for (const [key, value] of Object.entries(a.literals)) {
        factors.push({ score: Math.abs(value - b.literals[key]) * function_signature_weights.literals, name: `${key} literals` });
    }

    return factors;
};

export const sum_factors = (factors: SimilarityFactor[]): number => {
    let similarity = 0;
    for (const factor of factors) { similarity += factor.score; }
    return similarity;
}

export type Signature = VariableSignature | FunctionSignature;

export const write_signature_data = (filename: string, signatures: { [key: string]: FunctionSignature }) => {
    fs.writeFileSync(filename, JSON.stringify(signatures, undefined, 2));
};

export const read_signature_data = (filename): {[key: string]: FunctionSignature } => {
    const data = fs.readFileSync(filename, "utf-8");
    return JSON.parse(data);
}
