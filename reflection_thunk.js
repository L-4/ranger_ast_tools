const USAGE_UNKNOWN = 0;
const USAGE_INT = 1 << 0;
const USAGE_FLOAT = 1 << 1;
const USAGE_STRING = 1 << 2;
const USAGE_BOOLEAN = 1 << 3
const USAGE_ARRAY = 1 << 4;
const USAGE_OBJECT = 1 << 5;
const USAGE_F32_ARRAY = 1 << 6;
const USAGE_I32_ARRAY = 1 << 7;
const USAGE_U32_ARRAY = 1 << 8;

const usage_type_to_string = [
  "int", "float", "string", "bool", "array", "object", "f32array", "i32array", "u32array",
];

/** @type {[key: string]: number} */
const value_types = {};
const param_types = {};
/**
 * @param {any} value
 * @param {string} name
 */
const value_into_types = (value) => {
  switch (typeof value) {
    case "boolean":
      return USAGE_BOOLEAN;
      break;

    case "string":
      return USAGE_STRING;
      break;

    case "number":
      if (value == (value | 0)) {
        return USAGE_INT;
      } else {
        return USAGE_FLOAT;
      }
      break;

    case "object":
      if (Array.isArray(value)) {
        return USAGE_ARRAY;
      } else if (value instanceof Float32Array) {
        return USAGE_F32_ARRAY;
      } else if (value instanceof Uint32Array) {
        return USAGE_U32_ARRAY;
      } else if (value instanceof Int32Array) {
        return USAGE_I32_ARRAY;
      } else {
        return USAGE_OBJECT;
      }
      break
  }
};

const value_set_as = (value, name) => {
  if (value_types[name] === undefined) {
    value_types[name] = USAGE_UNKNOWN;
  }

  value_types[name] |= value_into_types(value);
}

const value_set_as_in_func = (value, name, func) => {
  value_set_as(value, name); // To be conservative, we always assign a global type.
  if (param_types[func] === undefined) {
    param_types[func] = {};
  }

  const params = param_types[func];

  if (params[name] === undefined) {
    params[name] = USAGE_UNKNOWN;
  }

  params[name] |= value_into_types(value);
};

// const dump_value_types = () => {
//   for (const [varname, types] of Object.entries(value_types)) {
//     if (types == USAGE_UNKNOWN) {
//       console.log(`'${varname}': no known types`);
//     } else {
//       const usages = [];
//       for (let i = 0; i < usage_type_to_string.length; ++i) {
//         const mask = 1 << i;
//         if ((types & mask) === mask) {
//           usages.push(usage_type_to_string[i]);
//         }
//       }
//       console.log(`'${varname}': used as ${usages.join(", ")}`);
//     }
//   }
// };
